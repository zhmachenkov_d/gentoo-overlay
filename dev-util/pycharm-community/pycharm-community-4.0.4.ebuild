# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit eutils

DESCRIPTION="Intelligent Python IDE with unique code assistance and analysis"
HOMEPAGE="http://www.jetbrains.com/pycharm/"
SRC_URI="http://download-cf.jetbrains.com/python/${P}.tar.gz"

LICENSE="Apache-2.0 BSD CDDL MIT-with-advertising"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=">=virtual/jre-1.6"
RDEPEND="${DEPEND}"

RESTRICT="mirror strip"

MY_PN=${PN/-community/}

src_install() {
		local DIR=/opt/${PN}
        insinto $DIR
        doins -r *
	
        fperms a+x ${DIR}/bin/{pycharm.sh,fsnotifier{,64},inspect.sh}

        dosym ${DIR}/bin/pycharm.sh /usr/bin/${PN}
		newicon "bin/${MY_PN}.png" "${PN}.png"
        make_desktop_entry ${PN} "PyCharm Community ${PV}" "${PN}" "Development;IDE"
}
